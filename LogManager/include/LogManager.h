#pragma once

class __declspec(dllexport) LogManager
{
public:
	static LogManager &GetInstance();

public:
	LogManager();
	virtual ~LogManager();

	void Regsiter(const char* sName);
	void UnRegsiter(const char* sName);

	void Log(const char* sName, int level, char* sFormat, ...);

public:
	enum { LOGINFO = 128, LOGWARNING = 32, LOGERROR = 16 };
};
