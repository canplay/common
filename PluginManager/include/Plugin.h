#pragma once

#define PLUGINVERSION 1

class __declspec(dllexport) Plugin
{
public:
	virtual ~Plugin() { }

	virtual int GetVersion() const { return PLUGINVERSION; }

	virtual int Init() { return 1; }
	virtual int Shutdown() { return 1; }
};
