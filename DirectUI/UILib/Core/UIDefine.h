#pragma once
//////////////BEGIN消息映射宏定义////////////////////////////////////////////////////
///

namespace UILib
{

enum UISig
{
	UISig_end = 0, // [marks end of message map]
	UISig_lwl,     // LRESULT (WPARAM, LPARAM)
	UISig_vn,      // void (TNotifyUI)
};

class CControlUI;

// Structure for notifications to the outside world
typedef struct tagTNotifyUI 
{
	CUIString sType;
	CUIString sVirtualWnd;
	CControlUI* pSender;
	DWORD dwTimestamp;
	POINT ptMouse;
	WPARAM wParam;
	LPARAM lParam;
} TNotifyUI;

class CNotifyPump;
typedef void (CNotifyPump::*UI_PMSG)(TNotifyUI& msg);  //指针类型

union UIMessageMapFunctions
{
	UI_PMSG pfn;   // generic member function pointer
	LRESULT (CNotifyPump::*pfn_Notify_lwl)(WPARAM, LPARAM);
	void (CNotifyPump::*pfn_Notify_vn)(TNotifyUI&);
};

//定义所有消息类型
//////////////////////////////////////////////////////////////////////////

#define UI_MSGTYPE_MENU                   (_T("menu"))
#define UI_MSGTYPE_LINK                   (_T("link"))

#define UI_MSGTYPE_TIMER                  (_T("timer"))
#define UI_MSGTYPE_CLICK                  (_T("click"))
#define UI_MSGTYPE_DBCLICK                (_T("dbclick"))

#define UI_MSGTYPE_RETURN                 (_T("return"))
#define UI_MSGTYPE_SCROLL                 (_T("scroll"))

#define UI_MSGTYPE_DROPDOWN               (_T("dropdown"))
#define UI_MSGTYPE_SETFOCUS               (_T("setfocus"))

#define UI_MSGTYPE_KILLFOCUS              (_T("killfocus"))
#define UI_MSGTYPE_ITEMCLICK 		   	   (_T("itemclick"))
#define UI_MSGTYPE_ITEMRCLICK 			   (_T("itemrclick"))
#define UI_MSGTYPE_TABSELECT              (_T("tabselect"))

#define UI_MSGTYPE_ITEMSELECT 		   	   (_T("itemselect"))
#define UI_MSGTYPE_ITEMEXPAND             (_T("itemexpand"))
#define UI_MSGTYPE_WINDOWINIT             (_T("windowinit"))
#define UI_MSGTYPE_BUTTONDOWN 		   	   (_T("buttondown"))
#define UI_MSGTYPE_MOUSEENTER			   (_T("mouseenter"))
#define UI_MSGTYPE_MOUSELEAVE			   (_T("mouseleave"))

#define UI_MSGTYPE_TEXTCHANGED            (_T("textchanged"))
#define UI_MSGTYPE_HEADERCLICK            (_T("headerclick"))
#define UI_MSGTYPE_ITEMDBCLICK            (_T("itemdbclick"))
#define UI_MSGTYPE_SHOWACTIVEX            (_T("showactivex"))

#define UI_MSGTYPE_ITEMCOLLAPSE           (_T("itemcollapse"))
#define UI_MSGTYPE_ITEMACTIVATE           (_T("itemactivate"))
#define UI_MSGTYPE_VALUECHANGED           (_T("valuechanged"))
#define UI_MSGTYPE_VALUECHANGED_MOVE      (_T("movevaluechanged"))

#define UI_MSGTYPE_SELECTCHANGED 		   (_T("selectchanged"))
#define UI_MSGTYPE_UNSELECTED	 		   (_T("unselected"))

//////////////////////////////////////////////////////////////////////////



struct UI_MSGMAP_ENTRY;
struct UI_MSGMAP
{
#ifndef UILIB_STATIC
	const UI_MSGMAP* (PASCAL* pfnGetBaseMap)();
#else
	const UI_MSGMAP* pBaseMap;
#endif
	const UI_MSGMAP_ENTRY* lpEntries;
};

//结构定义
struct UI_MSGMAP_ENTRY //定义一个结构体，来存放消息信息
{
	CUIString sMsgType;          // UI消息类型
	CUIString sCtrlName;         // 控件名称
	UINT       nSig;              // 标记函数指针类型
	UI_PMSG   pfn;               // 指向函数的指针
};

//定义
#ifndef UILIB_STATIC
#define UI_DECLARE_MESSAGE_MAP()                                         \
private:                                                                  \
	static const UI_MSGMAP_ENTRY _messageEntries[];                      \
protected:                                                                \
	static const UI_MSGMAP messageMap;                                   \
	static const UI_MSGMAP* PASCAL _GetBaseMessageMap();                 \
	virtual const UI_MSGMAP* GetMessageMap() const;                      \

#else
#define UI_DECLARE_MESSAGE_MAP()                                         \
private:                                                                  \
	static const UI_MSGMAP_ENTRY _messageEntries[];                      \
protected:                                                                \
	static  const UI_MSGMAP messageMap;				                  \
	virtual const UI_MSGMAP* GetMessageMap() const;                      \

#endif


//基类声明开始
#ifndef UILIB_STATIC
#define UI_BASE_BEGIN_MESSAGE_MAP(theClass)                              \
	const UI_MSGMAP* PASCAL theClass::_GetBaseMessageMap()               \
		{ return NULL; }                                                  \
	const UI_MSGMAP* theClass::GetMessageMap() const                     \
		{ return &theClass::messageMap; }                                 \
	UILIB_COMDAT const UI_MSGMAP theClass::messageMap =                  \
		{  &theClass::_GetBaseMessageMap, &theClass::_messageEntries[0] };\
	UILIB_COMDAT const UI_MSGMAP_ENTRY theClass::_messageEntries[] =     \
	{                                                                     \

#else
#define UI_BASE_BEGIN_MESSAGE_MAP(theClass)                              \
	const UI_MSGMAP* theClass::GetMessageMap() const                     \
		{ return &theClass::messageMap; }                                 \
	UILIB_COMDAT const UI_MSGMAP theClass::messageMap =                  \
		{  NULL, &theClass::_messageEntries[0] };                         \
	UILIB_COMDAT const UI_MSGMAP_ENTRY theClass::_messageEntries[] =     \
	{                                                                     \

#endif


//子类声明开始
#ifndef UILIB_STATIC
#define UI_BEGIN_MESSAGE_MAP(theClass, baseClass)                        \
	const UI_MSGMAP* PASCAL theClass::_GetBaseMessageMap()               \
		{ return &baseClass::messageMap; }                                \
	const UI_MSGMAP* theClass::GetMessageMap() const                     \
		{ return &theClass::messageMap; }                                 \
	UILIB_COMDAT const UI_MSGMAP theClass::messageMap =                  \
		{ &theClass::_GetBaseMessageMap, &theClass::_messageEntries[0] }; \
	UILIB_COMDAT const UI_MSGMAP_ENTRY theClass::_messageEntries[] =     \
	{                                                                     \

#else
#define UI_BEGIN_MESSAGE_MAP(theClass, baseClass)                        \
	const UI_MSGMAP* theClass::GetMessageMap() const                     \
		{ return &theClass::messageMap; }                                 \
	UILIB_COMDAT const UI_MSGMAP theClass::messageMap =                  \
		{ &baseClass::messageMap, &theClass::_messageEntries[0] };        \
	UILIB_COMDAT const UI_MSGMAP_ENTRY theClass::_messageEntries[] =     \
	{                                                                     \

#endif


//声明结束
#define UI_END_MESSAGE_MAP()                                             \
	{ _T(""), _T(""), UISig_end, (UI_PMSG)0 }                           \
};                                                                        \


//定义消息类型--执行函数宏
#define UI_ON_MSGTYPE(msgtype, memberFxn)                                \
	{ msgtype, _T(""), UISig_vn, (UI_PMSG)&memberFxn},                  \


//定义消息类型--控件名称--执行函数宏
#define UI_ON_MSGTYPE_CTRNAME(msgtype,ctrname,memberFxn)                 \
	{ msgtype, ctrname, UISig_vn, (UI_PMSG)&memberFxn },                \


//定义click消息的控件名称--执行函数宏
#define UI_ON_CLICK_CTRNAME(ctrname,memberFxn)                           \
	{ UI_MSGTYPE_CLICK, ctrname, UISig_vn, (UI_PMSG)&memberFxn },      \


//定义selectchanged消息的控件名称--执行函数宏
#define UI_ON_SELECTCHANGED_CTRNAME(ctrname,memberFxn)                   \
    { UI_MSGTYPE_SELECTCHANGED,ctrname,UISig_vn,(UI_PMSG)&memberFxn }, \


//定义killfocus消息的控件名称--执行函数宏
#define UI_ON_KILLFOCUS_CTRNAME(ctrname,memberFxn)                       \
	{ UI_MSGTYPE_KILLFOCUS,ctrname,UISig_vn,(UI_PMSG)&memberFxn },     \


//定义menu消息的控件名称--执行函数宏
#define UI_ON_MENU_CTRNAME(ctrname,memberFxn)                            \
	{ UI_MSGTYPE_MENU,ctrname,UISig_vn,(UI_PMSG)&memberFxn },          \


//定义与控件名称无关的消息宏

  //定义timer消息--执行函数宏
#define UI_ON_TIMER()                                                    \
	{ UI_MSGTYPE_TIMER, _T(""), UISig_vn,(UI_PMSG)&OnTimer },          \


///
//////////////END消息映射宏定义////////////////////////////////////////////////////


//////////////BEGIN控件名称宏定义//////////////////////////////////////////////////
///

#define  UI_CTR_EDIT                            (_T("Edit"))
#define  UI_CTR_LIST                            (_T("List"))
#define  UI_CTR_TEXT                            (_T("Text"))

#define  UI_CTR_COMBO                           (_T("Combo"))
#define  UI_CTR_LABEL                           (_T("Label"))
#define  UI_CTR_FLASH							 (_T("Flash"))

#define  UI_CTR_BUTTON                          (_T("Button"))
#define  UI_CTR_OPTION                          (_T("Option"))
#define  UI_CTR_SLIDER                          (_T("Slider"))

#define  UI_CTR_CONTROL                         (_T("Control"))
#define  UI_CTR_ACTIVEX                         (_T("ActiveX"))
#define  UI_CTR_GIFANIM                         (_T("GifAnim"))

#define  UI_CTR_LISTITEM                        (_T("ListItem"))
#define  UI_CTR_PROGRESS                        (_T("Progress"))
#define  UI_CTR_RICHEDIT                        (_T("RichEdit"))
#define  UI_CTR_CHECKBOX                        (_T("CheckBox"))
#define  UI_CTR_COMBOBOX                        (_T("ComboBox"))
#define  UI_CTR_DATETIME                        (_T("DateTime"))
#define  UI_CTR_TREEVIEW                        (_T("TreeView"))
#define  UI_CTR_TREENODE                        (_T("TreeNode"))

#define  UI_CTR_CONTAINER                       (_T("Container"))
#define  UI_CTR_TABLAYOUT                       (_T("TabLayout"))
#define  UI_CTR_SCROLLBAR                       (_T("ScrollBar"))

#define  UI_CTR_LISTHEADER                      (_T("ListHeader"))
#define  UI_CTR_TILELAYOUT                      (_T("TileLayout"))
#define  UI_CTR_WEBBROWSER                      (_T("WebBrowser"))

#define  UI_CTR_CHILDLAYOUT                     (_T("ChildLayout"))
#define  UI_CTR_LISTELEMENT                     (_T("ListElement"))

#define  UI_CTR_DIALOGLAYOUT                    (_T("DialogLayout"))

#define  UI_CTR_VERTICALLAYOUT                  (_T("VerticalLayout"))
#define  UI_CTR_LISTHEADERITEM                  (_T("ListHeaderItem"))

#define  UI_CTR_LISTTEXTELEMENT                 (_T("ListTextElement"))

#define  UI_CTR_HORIZONTALLAYOUT                (_T("HorizontalLayout"))
#define  UI_CTR_LISTLABELELEMENT                (_T("ListLabelElement"))

#define  UI_CTR_LISTCONTAINERELEMENT            (_T("ListContainerElement"))

///
//////////////END控件名称宏定义//////////////////////////////////////////////////


}// namespace UILib

