#pragma once

#ifdef FLASH_EXPORTS
#define FLASH_API __declspec(dllexport)
#else
#define FLASH_API __declspec(dllimport)
#endif

#include "stdafx.h"
#include "flashwnd.h"

struct _WindowInfo
{
	LPCWSTR title;
	LPCWSTR flash;
	SIZE size;
	POINT position;

	_WindowInfo::_WindowInfo()
	{
		title = L"FlashUI Window";
		flash = 0;
		size.cx = size.cy = 0;
		position.x = position.y = 0;
	}
};

class FlashUIWindow
{
private:
	FlashUIWindow();

public:
	static FlashUIWindow * _InterfaceGet();

	bool WINAPI Create(_WindowInfo windowInfo, HINSTANCE hInstance, PSTR szCmdLine, int iCmdShow, FlashFuncCallback ffc);
	void WINAPI Loop();
	void WINAPI Close();
	void WINAPI Command(LPCWSTR name, LPCWSTR args);

	LRESULT MessageLoop(HWND hWnd, UINT nMsg, WPARAM wParam, LPARAM lParam);

private:
	HRESULT LoadFromResource(UINT id, LPCWSTR resType);

	int m_nBitsPixel;

	HINSTANCE m_hInstance;
	HWND m_hWnd;

	CFlashWnd* m_pFlashWnd;
};

extern "C" { FLASH_API FlashUIWindow* WINAPI FlashUICreate(int key); };