#pragma once

#include "sol.hpp"
#include <string>

class __declspec(dllexport) LuaManager
{
public:
	static LuaManager &GetInstance();

public:
	LuaManager();
	virtual ~LuaManager();

	void Init();
	void Loop();
	void Shutdown();

	sol::state& GetLua() { return m_Lua; }

private:
	sol::state m_Lua;
};

