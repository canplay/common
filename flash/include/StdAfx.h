#pragma once

#include <SDKDDKVer.h>
#include <windows.h>

struct   __declspec(uuid("9C59509A-39BD-11D1-8C4A-00C04FD930C5"))   IDirectDraw4;
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <assert.h>
#include <ole2.h>
#include <ddraw.h>

#ifndef DEFINE_GUID2
#define DEFINE_GUID2(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8) \
        const GUID name \
                = { l, w1, w2, { b1, b2,  b3,  b4,  b5,  b6,  b7,  b8 } }
#endif	   
