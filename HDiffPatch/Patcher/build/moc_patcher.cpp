/****************************************************************************
** Meta object code from reading C++ file 'patcher.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../patcher.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'patcher.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Patcher_t {
    QByteArrayData data[10];
    char stringdata0[188];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Patcher_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Patcher_t qt_meta_stringdata_Patcher = {
    {
QT_MOC_LITERAL(0, 0, 7), // "Patcher"
QT_MOC_LITERAL(1, 8, 18), // "on_btn_old_clicked"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 18), // "on_btn_new_clicked"
QT_MOC_LITERAL(4, 47, 21), // "on_btn_output_clicked"
QT_MOC_LITERAL(5, 69, 23), // "on_btn_generate_clicked"
QT_MOC_LITERAL(6, 93, 20), // "on_btn_patch_clicked"
QT_MOC_LITERAL(7, 114, 23), // "on_edit_old_textChanged"
QT_MOC_LITERAL(8, 138, 23), // "on_edit_new_textChanged"
QT_MOC_LITERAL(9, 162, 25) // "on_edit_ouput_textChanged"

    },
    "Patcher\0on_btn_old_clicked\0\0"
    "on_btn_new_clicked\0on_btn_output_clicked\0"
    "on_btn_generate_clicked\0on_btn_patch_clicked\0"
    "on_edit_old_textChanged\0on_edit_new_textChanged\0"
    "on_edit_ouput_textChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Patcher[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x08 /* Private */,
       3,    0,   55,    2, 0x08 /* Private */,
       4,    0,   56,    2, 0x08 /* Private */,
       5,    0,   57,    2, 0x08 /* Private */,
       6,    0,   58,    2, 0x08 /* Private */,
       7,    0,   59,    2, 0x08 /* Private */,
       8,    0,   60,    2, 0x08 /* Private */,
       9,    0,   61,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Patcher::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Patcher *_t = static_cast<Patcher *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_btn_old_clicked(); break;
        case 1: _t->on_btn_new_clicked(); break;
        case 2: _t->on_btn_output_clicked(); break;
        case 3: _t->on_btn_generate_clicked(); break;
        case 4: _t->on_btn_patch_clicked(); break;
        case 5: _t->on_edit_old_textChanged(); break;
        case 6: _t->on_edit_new_textChanged(); break;
        case 7: _t->on_edit_ouput_textChanged(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject Patcher::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Patcher.data,
      qt_meta_data_Patcher,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Patcher::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Patcher::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Patcher.stringdata0))
        return static_cast<void*>(const_cast< Patcher*>(this));
    return QDialog::qt_metacast(_clname);
}

int Patcher::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
