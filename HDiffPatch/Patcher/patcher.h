#ifndef PATCHER_H
#define PATCHER_H

#include <QtWidgets/QDialog>
#include "ui_patcher.h"

class Patcher : public QDialog
{
	Q_OBJECT

public:
	Patcher(QWidget *parent = 0);
	~Patcher();

private:
	Ui::PatcherClass ui;

private slots:
	void on_btn_old_clicked();
	void on_btn_new_clicked();
	void on_btn_output_clicked();
	void on_btn_generate_clicked();
	void on_btn_patch_clicked();

	void on_edit_old_textChanged();
	void on_edit_new_textChanged();
	void on_edit_ouput_textChanged();
};

#endif // PATCHER_H
