#include "stdafx.h"
#include "flash.h"

#define WINDOW_CLASS_NAME L"FlashUI_HostWindow_Application"
FlashUIWindow* g_pWindow = 0;

HINSTANCE app_instance_ = NULL;
wchar_t flashname[256];
typedef int (WINAPI *GDC_FUNC)(HDC hdc, int nIndex);
GDC_FUNC sys_org_func;

BYTE jmp_cmd[5];
BYTE sys_func_enter[5];


FlashUIWindow* WINAPI FlashUICreate(int key)
{
	if (key == 1234567890)
		return FlashUIWindow::_InterfaceGet();

	return 0;
}

FlashUIWindow* FlashUIWindow::_InterfaceGet()
{
	if (!g_pWindow) g_pWindow = new FlashUIWindow();
	return g_pWindow;
}

FlashUIWindow::FlashUIWindow()
{

}

LRESULT CALLBACK WndProc(HWND hWnd, UINT nMsg, WPARAM wParam, LPARAM lParam)
{
	return g_pWindow->MessageLoop(hWnd, nMsg, wParam, lParam);
}

int WINAPI cjh_GetDeviceCaps(HDC hdc, int nIndex)
{
	int rst = 0;

	if (BITSPIXEL == nIndex) rst = 32;
	else
	{
		memcpy((LPVOID)sys_org_func, sys_func_enter, 5);
		FlushInstructionCache(GetCurrentProcess(), (LPVOID)sys_org_func, 5);
		rst = GetDeviceCaps(hdc, nIndex);
		memcpy((LPVOID)sys_org_func, jmp_cmd, 5);
		FlushInstructionCache(GetCurrentProcess(), (LPVOID)sys_org_func, 5);
	}

	return rst;
}

bool FlashUIWindow::Create(_WindowInfo windowInfo, HINSTANCE hInstance, PSTR szCmdLine, int iCmdShow, FlashFuncCallback ffc)
{
	app_instance_ = hInstance;
	swprintf_s(flashname, windowInfo.flash);

	WNDCLASSEX wcex = { 0 };
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wcex.hCursor = LoadCursor(hInstance, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszClassName = WINDOW_CLASS_NAME;
	RegisterClassEx(&wcex);

	windowInfo.position.x = (GetSystemMetrics(SM_CXSCREEN) - windowInfo.size.cx) / 2;
	windowInfo.position.y = (GetSystemMetrics(SM_CYSCREEN) - windowInfo.size.cy) / 2;
	HWND hWnd = CreateWindow(WINDOW_CLASS_NAME, windowInfo.title, WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		windowInfo.position.x, windowInfo.position.y, windowInfo.size.cx, windowInfo.size.cy, 0, 0, hInstance, 0);
	if (!hWnd)
	{
		Close();
		return false;
	}
	UpdateWindow(hWnd);

	OleInitialize(NULL);
	m_nBitsPixel = GetDeviceCaps(0, BITSPIXEL);
	if (32 != m_nBitsPixel)
	{
		HMODULE hDll = GetModuleHandle(_T("gdi32.dll"));

		if (!hDll)
		{
			hDll = LoadLibrary(_T("gdi32.dll"));
			if (!hDll) return 0;
		}

		sys_org_func = (GDC_FUNC)(GetProcAddress(hDll, "GetDeviceCaps"));
		memcpy(sys_func_enter, (LPVOID)sys_org_func, 5);
		jmp_cmd[0] = 0xE9;
		*(int*)&jmp_cmd[1] = (int)&cjh_GetDeviceCaps - ((int)sys_org_func + 5);

		DWORD dwOld = 0;
		if (!VirtualProtect(sys_org_func, 5, PAGE_EXECUTE_READWRITE, &dwOld))
			return S_FALSE;

		memcpy(sys_org_func, jmp_cmd, 5);
		FlushInstructionCache(GetCurrentProcess(), (LPVOID)sys_org_func, 5);
	}

	m_pFlashWnd = new CFlashWnd;
	m_pFlashWnd->Create(windowInfo.title, ShockwaveFlashObjects::CLSID_ShockwaveFlash,
		WS_EX_LAYERED, WS_POPUP | WS_VISIBLE | WS_CLIPSIBLINGS,
		hWnd, hInstance);
	m_pFlashWnd->SetFlashCallback(ffc);

	m_hInstance = hInstance;
	m_hWnd = hWnd;

	return true;
}

void FlashUIWindow::Loop()
{
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

void FlashUIWindow::Close()
{
	DestroyWindow(m_hWnd);

	delete m_pFlashWnd;
	OleUninitialize();

	if (32 != m_nBitsPixel)
	{
		memcpy((LPVOID)sys_org_func, sys_func_enter, 5);
		FlushInstructionCache(GetCurrentProcess(), (LPVOID)sys_org_func, 5);
	}

	UnregisterClass(WINDOW_CLASS_NAME, m_hInstance);
}

void FlashUIWindow::Command(LPCWSTR name, LPCWSTR args)
{
	wchar_t request[255];
	swprintf_s(request, L"<invoke name = \"%s\"><arguments><string>%s</string></arguments></invoke>", name, args);
	m_pFlashWnd->FlashCall(request);
}

LRESULT FlashUIWindow::MessageLoop(HWND hWnd, UINT nMsg, WPARAM wParam, LPARAM lParam)
{
	switch (nMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_MOVE:
	{
		RECT r;
		GetWindowRect(hWnd, &r);
		if (m_pFlashWnd)
			SetWindowPos(m_pFlashWnd->GetHWND(), NULL, r.left, r.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
	}
	break;

	case WM_SIZE:
	{
		RECT r;
		GetWindowRect(hWnd, &r);
		if (m_pFlashWnd)
			SetWindowPos(m_pFlashWnd->GetHWND(), NULL, 0, 0, (r.right - r.left), (r.bottom - r.top), SWP_NOMOVE | SWP_NOZORDER);
	}
	break;

	default:
		return DefWindowProc(hWnd, nMsg, wParam, lParam);
	}
	return 0;
}

HRESULT FlashUIWindow::LoadFromResource(UINT id, LPCWSTR resType)
{
	if (m_hInstance == NULL)
		return S_FALSE;

	HRSRC hResInfo = ::FindResource(m_hInstance, MAKEINTRESOURCE(id), resType);
	if (!hResInfo)
		return S_FALSE;

	HRSRC hResource = ::FindResource(m_hInstance, MAKEINTRESOURCE(id), resType);
	if (!hResource)
		return S_FALSE;

	HGLOBAL hResourceData = ::LoadResource(m_hInstance, hResource);
	if (!hResourceData)
		return S_FALSE;

	LPVOID lpResourceData = ::LockResource(hResourceData);
	if (!lpResourceData)
		return S_FALSE;

	DWORD dwResourceSize = ::SizeofResource(m_hInstance, hResource);
	if (dwResourceSize == 0)
		return S_FALSE;

	HGLOBAL hGlobalData = ::GlobalAlloc(GPTR, dwResourceSize + 8);
	if (!hGlobalData)
		return S_FALSE;

	LPBYTE pMem = (LPBYTE)::GlobalLock(hGlobalData);
	if (!pMem)
		return S_FALSE;

	((long*)pMem)[0] = 0x55665566;
	((long*)pMem)[1] = dwResourceSize;
	::CopyMemory((void*)(&(pMem[8])), lpResourceData, dwResourceSize);
	IStream* pStream = 0;
	HRESULT hr = ::CreateStreamOnHGlobal(hGlobalData, TRUE, &pStream);
	if (hr == S_OK)
	{
		IPersistStreamInit* pPersistStream = NULL;
		hr = m_pFlashWnd->QueryInterface(IID_IPersistStreamInit, (void**)&pPersistStream);
		if (hr == S_OK)
			hr = pPersistStream->Load(pStream);
		pPersistStream->Release();
		pPersistStream = NULL;
		pStream->Release();
		pStream = NULL;
	}
	::GlobalUnlock((HGLOBAL)pMem);

	return hr;
}
