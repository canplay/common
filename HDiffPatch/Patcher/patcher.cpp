#include "patcher.h"
#include "QtWidgets\QMessageBox"
#include "QFileDialog"

Patcher::Patcher(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
}

Patcher::~Patcher()
{

}

void Patcher::on_btn_old_clicked()
{
	QString s = QFileDialog::getOpenFileName(this, u8"选择文件", "/", u8"所有文件(*.*)");
	ui.edit_old->setPlainText(s);
}

void Patcher::on_btn_new_clicked()
{
	QString s = QFileDialog::getOpenFileName(this, u8"选择文件", "/", u8"所有文件(*.*)");
	ui.edit_new->setPlainText(s);
}

void Patcher::on_btn_output_clicked()
{
	QString s = QFileDialog::getSaveFileName(this, u8"选择文件", "/", u8"补丁文件(*.patch)");
	ui.edit_output->setPlainText(s);
}

void Patcher::on_btn_generate_clicked()
{

}

void Patcher::on_btn_patch_clicked()
{

}

void Patcher::on_edit_old_textChanged()
{
	if (!ui.edit_old->toPlainText().isEmpty() &&
		!ui.edit_new->toPlainText().isEmpty() &&
		!ui.edit_output->toPlainText().isEmpty())
		ui.btn_generate->setEnabled(true);
	else ui.btn_generate->setEnabled(false);
}

void Patcher::on_edit_new_textChanged()
{
	if (!ui.edit_old->toPlainText().isEmpty() &&
		!ui.edit_new->toPlainText().isEmpty() &&
		!ui.edit_output->toPlainText().isEmpty())
		ui.btn_generate->setEnabled(true);
	else ui.btn_generate->setEnabled(false);
}

void Patcher::on_edit_ouput_textChanged()
{
	if (!ui.edit_old->toPlainText().isEmpty() &&
		!ui.edit_new->toPlainText().isEmpty() &&
		!ui.edit_output->toPlainText().isEmpty())
		ui.btn_generate->setEnabled(true);
	else ui.btn_generate->setEnabled(false);
}
