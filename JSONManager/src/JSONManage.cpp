#include "JSONManager.h"
#include <LogManager.h>

JSONManager &JSONManager::GetInstance()
{
	static JSONManager instance;
	return instance;
}

JSONManager::JSONManager()
{
	LogManager::GetInstance().Regsiter("JSONManager");
	LogManager::GetInstance().Log("JSONManager", LogManager::LOGINFO, "初始化JSON管理器");
}

JSONManager::~JSONManager()
{
	LogManager::GetInstance().Log("JSONManager", LogManager::LOGINFO, "关闭JSON管理器");
}