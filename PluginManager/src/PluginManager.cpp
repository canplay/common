#include "PluginManager.h"
#include <LogManager.h>
#include <direct.h>
#include <io.h>

PluginManager &PluginManager::GetInstance()
{
	static PluginManager instance;
	return instance;
}

PluginManager::PluginManager(void)
{
	LogManager::GetInstance().Regsiter("PluginManager");
	LogManager::GetInstance().Log("PluginManager", LogManager::LOGINFO, "初始化插件管理器");

	plugins = new std::vector<_Plugin>();
}

PluginManager::~PluginManager(void)
{
	// release plugins
	std::vector<_Plugin>::reverse_iterator it = plugins->rbegin();
	while (it != plugins->rend())
	{
		if ((*it).plugin == 0) continue;
		(*it).release_plugin((*it).plugin);

		LogManager::GetInstance().Log("PluginManager", LogManager::LOGINFO, "卸载插件 %s .", (*it).name.c_str());

		++it;
	}

	plugins->clear();
	plugins->shrink_to_fit();
	delete plugins;
	plugins = 0;

	LogManager::GetInstance().Log("PluginManager", LogManager::LOGINFO, "关闭插件管理器");
}

int PluginManager::Load(const char *name, int init)
{
	// find plugins
	for (size_t i = 0; i < plugins->size(); i++)
	{
		const _Plugin &p = (*plugins)[i];
		if (p.name == name) return 0;
	}

	// create plugin
	_Plugin p;
	p.name = name;
	p.plugin = NULL;
	p.handle = NULL;
	p.create_plugin = NULL;
	p.release_plugin = NULL;

	// plugin library name
	char currPath[1024];
	_getcwd(currPath, 1024);
	sprintf_s(currPath, "%s/Plugins/%s.dll", currPath, name);

	p.handle = LoadLibrary(currPath);
	if (p.handle != NULL)
	{
		p.create_plugin = (CreatePlugin)GetProcAddress(p.handle, "CreatePlugin");
		p.release_plugin = (ReleasePlugin)GetProcAddress(p.handle, "ReleasePlugin");
	}
	else return 0;

	if (p.create_plugin == NULL) return 0;
	if (p.release_plugin == NULL) return 0;

	// create plugin
	p.plugin = (Plugin*)p.create_plugin();
	if (p.plugin == NULL) return 0;

	// check version
	if (p.plugin->GetVersion() != PLUGINVERSION)
	{
		p.release_plugin(p.plugin);
		p.plugin = NULL;
		return 0;
	}

	// initialize plugin
	if (init && p.plugin->Init() == 0)
	{
		p.release_plugin(p.plugin);
		p.plugin = NULL;
		return 0;
	}

	plugins->push_back(p);

	LogManager::GetInstance().Log("PluginManager", LogManager::LOGINFO, "载入插件 %s .", p.name.c_str());

	return 1;
}

void PluginManager::Unload(const char *name)
{
	for (std::vector<_Plugin>::iterator it = plugins->begin(); it != plugins->end(); it++)
	{
		if ((*it).plugin == 0) continue;

		if ((*it).name == name)
		{
			(*it).plugin->Shutdown();

			LogManager::GetInstance().Log("PluginManager", LogManager::LOGINFO, "关闭插件 %s .", (*it).name.c_str());
		}
	}
}

void PluginManager::Init()
{
	long handle;
	struct _finddata_t fileinfo;
	char currPath[1024];
	_getcwd(currPath, 1024);
	sprintf_s(currPath, "%s/Plugins/*.dll", currPath);
	handle = _findfirst(currPath, &fileinfo);
	if (-1 == handle) return;

	std::string filename = fileinfo.name;
	filename = filename.substr(0, filename.size() - 4);
	this->Load(filename.c_str(), 1);

	while (!_findnext(handle, &fileinfo))
	{
		filename = fileinfo.name;
		filename = filename.substr(0, filename.size() - 4);
		this->Load(filename.c_str(), 1);
	}
	_findclose(handle);
}

void PluginManager::Shutdown()
{
	std::vector<_Plugin>::reverse_iterator it = plugins->rbegin();
	while (it != plugins->rend())
	{
		if ((*it).plugin == 0) continue;
		(*it).plugin->Shutdown();

		LogManager::GetInstance().Log("PluginManager", LogManager::LOGINFO, "关闭插件 %s .", (*it).name.c_str());

		++it;
	}
}
