#pragma once

#include "document.h"
#include "stringbuffer.h"
#include "writer.h"

class __declspec(dllexport) JSONManager
{
public:
	static JSONManager &GetInstance();

public:
	JSONManager();
	virtual ~JSONManager();

	rapidjson::Document& GetDocument() { return m_Docment; };

private:
	rapidjson::Document m_Docment;
};
