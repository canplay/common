/********************************************************************************
** Form generated from reading UI file 'patcher.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PATCHER_H
#define UI_PATCHER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_PatcherClass
{
public:
    QGroupBox *groupBox;
    QTextEdit *edit_old;
    QPushButton *btn_old;
    QGroupBox *groupBox_2;
    QTextEdit *edit_new;
    QPushButton *btn_new;
    QGroupBox *groupBox_3;
    QTextEdit *edit_output;
    QPushButton *btn_output;
    QPushButton *btn_generate;
    QPushButton *btn_patch;
    QLabel *lbl_status;

    void setupUi(QDialog *PatcherClass)
    {
        if (PatcherClass->objectName().isEmpty())
            PatcherClass->setObjectName(QStringLiteral("PatcherClass"));
        PatcherClass->resize(561, 255);
        PatcherClass->setMinimumSize(QSize(561, 255));
        PatcherClass->setMaximumSize(QSize(561, 255));
        groupBox = new QGroupBox(PatcherClass);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(10, 10, 541, 61));
        edit_old = new QTextEdit(groupBox);
        edit_old->setObjectName(QStringLiteral("edit_old"));
        edit_old->setGeometry(QRect(10, 20, 441, 31));
        btn_old = new QPushButton(groupBox);
        btn_old->setObjectName(QStringLiteral("btn_old"));
        btn_old->setGeometry(QRect(460, 20, 75, 31));
        groupBox_2 = new QGroupBox(PatcherClass);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 80, 541, 61));
        edit_new = new QTextEdit(groupBox_2);
        edit_new->setObjectName(QStringLiteral("edit_new"));
        edit_new->setGeometry(QRect(10, 20, 441, 31));
        btn_new = new QPushButton(groupBox_2);
        btn_new->setObjectName(QStringLiteral("btn_new"));
        btn_new->setGeometry(QRect(460, 20, 75, 31));
        groupBox_3 = new QGroupBox(PatcherClass);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(10, 150, 541, 61));
        edit_output = new QTextEdit(groupBox_3);
        edit_output->setObjectName(QStringLiteral("edit_output"));
        edit_output->setGeometry(QRect(10, 20, 441, 31));
        btn_output = new QPushButton(groupBox_3);
        btn_output->setObjectName(QStringLiteral("btn_output"));
        btn_output->setGeometry(QRect(460, 20, 75, 31));
        btn_generate = new QPushButton(PatcherClass);
        btn_generate->setObjectName(QStringLiteral("btn_generate"));
        btn_generate->setEnabled(false);
        btn_generate->setGeometry(QRect(370, 220, 75, 31));
        btn_patch = new QPushButton(PatcherClass);
        btn_patch->setObjectName(QStringLiteral("btn_patch"));
        btn_patch->setEnabled(false);
        btn_patch->setGeometry(QRect(470, 220, 75, 31));
        lbl_status = new QLabel(PatcherClass);
        lbl_status->setObjectName(QStringLiteral("lbl_status"));
        lbl_status->setGeometry(QRect(10, 230, 54, 12));

        retranslateUi(PatcherClass);

        QMetaObject::connectSlotsByName(PatcherClass);
    } // setupUi

    void retranslateUi(QDialog *PatcherClass)
    {
        PatcherClass->setWindowTitle(QApplication::translate("PatcherClass", "Patcher", 0));
        groupBox->setTitle(QApplication::translate("PatcherClass", "\346\227\247\347\211\210\346\226\207\344\273\266", 0));
        edit_old->setPlaceholderText(QApplication::translate("PatcherClass", "\346\227\247\347\211\210\346\226\207\344\273\266\350\276\223\345\205\245\350\267\257\345\276\204", 0));
        btn_old->setText(QApplication::translate("PatcherClass", "\346\265\217\350\247\210...", 0));
        groupBox_2->setTitle(QApplication::translate("PatcherClass", "\346\226\260\347\211\210\346\226\207\344\273\266", 0));
        edit_new->setPlaceholderText(QApplication::translate("PatcherClass", "\346\226\260\347\211\210\346\226\207\344\273\266\350\276\223\345\205\245\350\267\257\345\276\204", 0));
        btn_new->setText(QApplication::translate("PatcherClass", "\346\265\217\350\247\210...", 0));
        groupBox_3->setTitle(QApplication::translate("PatcherClass", "\350\241\245\344\270\201\346\226\207\344\273\266", 0));
        edit_output->setPlaceholderText(QApplication::translate("PatcherClass", "\350\241\245\344\270\201\346\226\207\344\273\266\350\276\223\345\207\272\350\267\257\345\276\204", 0));
        btn_output->setText(QApplication::translate("PatcherClass", "\346\265\217\350\247\210...", 0));
        btn_generate->setText(QApplication::translate("PatcherClass", "\347\224\237\346\210\220\350\241\245\344\270\201", 0));
        btn_patch->setText(QApplication::translate("PatcherClass", "\345\256\211\350\243\205\350\241\245\344\270\201", 0));
        lbl_status->setText(QApplication::translate("PatcherClass", "\347\212\266\346\200\201", 0));
    } // retranslateUi

};

namespace Ui {
    class PatcherClass: public Ui_PatcherClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PATCHER_H
