#include "LuaManager.h"
#include <LogManager.h>
#include <direct.h>

LuaManager &LuaManager::GetInstance()
{
	static LuaManager instance;
	return instance;
}

LuaManager::LuaManager()
{
	LogManager::GetInstance().Regsiter("LuaManager");
	LogManager::GetInstance().Log("LuaManager", LogManager::LOGINFO, "初始化脚本管理器");

	m_Lua.open_libraries();

	m_Lua.set_function("log", [](const sol::variadic_args& va){
		std::string logs;

		for (auto v : va) logs += v;

		LogManager::GetInstance().Log("LuaManager", LogManager::LOGINFO, "%s", logs.c_str());
	});

	char currPath[1024];
	_getcwd(currPath, 1024);
	std::string sFilePath = currPath;
	sFilePath.append("/Main.lua");
	m_Lua.do_file(sFilePath);
}

LuaManager::~LuaManager()
{
	LogManager::GetInstance().Log("LuaManager", LogManager::LOGINFO, "关闭脚本管理器");
}

void LuaManager::Init()
{
	LogManager::GetInstance().Log("LuaManager", LogManager::LOGINFO, "调用脚本函数 Init()");
	m_Lua["Init"]();
}

void LuaManager::Loop()
{
	LogManager::GetInstance().Log("LuaManager", LogManager::LOGINFO, "调用脚本函数 Loop()");
	m_Lua["Loop"]();
}

void LuaManager::Shutdown()
{
	LogManager::GetInstance().Log("LuaManager", LogManager::LOGINFO, "调用脚本函数 Shutdown()");
	m_Lua["Shutdown"]();
}