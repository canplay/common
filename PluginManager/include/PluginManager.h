#pragma once

#include "Plugin.h"
#include <string>
#include <vector>
#include <windows.h>

class __declspec(dllexport) PluginManager
{
public:
	static PluginManager &GetInstance();

public:
	PluginManager();
	virtual ~PluginManager();

	int Load(const char *name, int init);
	void Unload(const char *name);

	void Init();
	void Shutdown();

private:
	typedef void *(*CreatePlugin)();
	typedef void (*ReleasePlugin)(void *plugin);

	struct _Plugin
	{
		std::string name;
		Plugin *plugin;
		HMODULE handle;
		CreatePlugin create_plugin;
		ReleasePlugin release_plugin;
	};

	std::vector<_Plugin>* plugins;
};
