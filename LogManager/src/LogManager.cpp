#include "LogManager.h"
#include "easylogging++.h"

LogManager &LogManager::GetInstance()
{
	static LogManager instance;
	return instance;
}

LogManager::LogManager()
{
	Regsiter("LogManager");
	Log("LogManager", LogManager::LOGINFO, "初始化日志管理器");
}

LogManager::~LogManager()
{
	Log("LogManager", LogManager::LOGINFO, "关闭日志管理器");

	std::vector<std::string> logger;
	el::Loggers::populateAllLoggerIds(&logger);

	for (std::vector<std::string>::iterator it = logger.begin(); it != logger.end(); ++it)
	{
		UnRegsiter(it->c_str());
	}
}

void LogManager::Regsiter(const char* sName)
{
	el::Loggers::getLogger(sName);

	el::Configurations conf;
	conf.setToDefault();
	conf.set(el::Level::Global, el::ConfigurationType::Format, "[%level| %datetime] | %msg");
	conf.set(el::Level::Global, el::ConfigurationType::Enabled, "true");
	conf.set(el::Level::Global, el::ConfigurationType::ToFile, "true");
	conf.set(el::Level::Global, el::ConfigurationType::ToStandardOutput, "true");
	conf.set(el::Level::Global, el::ConfigurationType::MillisecondsWidth, "3");
	conf.set(el::Level::Global, el::ConfigurationType::PerformanceTracking, "false");
	conf.set(el::Level::Global, el::ConfigurationType::MaxLogFileSize, "2097152");
	conf.set(el::Level::Global, el::ConfigurationType::LogFlushThreshold, "100");

	char logFileName[MAX_PATH];
	sprintf_s(logFileName, "Logs\\Log_%s.log", sName);

	conf.set(el::Level::Global, el::ConfigurationType::Filename, logFileName);

	conf.set(el::Level::Trace, el::ConfigurationType::Enabled, "false");
	conf.set(el::Level::Debug, el::ConfigurationType::Enabled, "false");
	conf.set(el::Level::Fatal, el::ConfigurationType::Enabled, "false");
	conf.set(el::Level::Verbose, el::ConfigurationType::Enabled, "false");

	el::Loggers::reconfigureLogger(sName, conf);
}

void LogManager::UnRegsiter(const char* sName)
{
// 	el::Loggers::unregisterLogger(sName);
}

void LogManager::Log(const char* sName, int level, char* sFormat, ...)
{
	if (!el::Loggers::hasLogger(sName)) return;

	va_list args;
	va_start(args, sFormat);

	int len = _vscprintf(sFormat, args) + 1;
	char *str = (char*)malloc(len * sizeof(char));
	vsprintf_s(str, len, sFormat, args);
	puts(str);

	va_end(args);

	switch (level)
	{
	case LOGINFO:
		CLOG(INFO, sName) << str;
		break;
	case LOGWARNING:
		CLOG(WARNING, sName) << str;
		break;
	case LOGERROR:
		CLOG(ERROR, sName) << str;
		break;
	default:
		CLOG(INFO, sName) << str;
		break;
	}

	free(str);
}